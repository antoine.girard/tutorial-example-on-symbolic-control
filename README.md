# Tutorial example on symbolic control

Matlab Scripts

Author: Antoine GIRARD, Université Paris-Saclay, CNRS, CentraleSupélec, Laboratoire des signaux et systèmes, 91190, Gif-sur-Yvette, France.

Date: 2024

These scripts provide an illustrative application of the symbolic control approach for controller synthesis.
These scripts are meant to be used for educational purpose.
