%% Tutorial example on symbolic control
%% Author: Antoine GIRARD, Université Paris-Saclay, CNRS, CentraleSupélec, Laboratoire des signaux et systèmes, 91190, Gif-sur-Yvette, France.
%% Date: 2024

function I=state2coord(i,p_x)

I=zeros(length(p_x)-1,1);

for k=1:length(p_x)-1
    I(k)=floor(mod(i-1,p_x(k+1))/p_x(k))+1; 
end
end


