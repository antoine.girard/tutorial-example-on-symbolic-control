%% Tutorial example on symbolic control
%% Author: Antoine GIRARD, Université Paris-Saclay, CNRS, CentraleSupélec, Laboratoire des signaux et systèmes, 91190, Gif-sur-Yvette, France.
%% Date: 2024


%% Definition of the dynamics of the system and of abstraction parameters

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% System Definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sampling time 
T=1;

% System dynamics
f=@(x,u,w) [x(1)+T*u(1)*cos(x(3))+T*w(1);...
            x(2)+T*u(1)*sin(x(3))+T*w(2);...
            x(3)+T*u(2)+T*w(3)];

% Bounds on Jacobian matrices for over-appoximation of reachable sets
Jf_x=@(u) [1 0 T*abs(u(1));...
           0 1 T*abs(u(1));...
           0 0 1];

Jf_w=@(u) [T 0 0;...
           0 T 0;...
           0 0 T];
       
% Constraints
bound_x=[0 10;0 10;-pi pi];                   % State Constraints
bound_u=[0.25 1;-1 1];                        % Control Constraints
bound_w=[-0.05 0.05;-0.05 0.05;-0.05 0.05];   % Disturbance Constraints


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstraction parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Symbolic parameters
n_x=[100;100;30];                        % Number of state partition cells
n_u=[3;5];                               % Number of control partition cells
