%% Tutorial example on symbolic control
%% Author: Antoine GIRARD, Université Paris-Saclay, CNRS, CentraleSupélec, Laboratoire des signaux et systèmes, 91190, Gif-sur-Yvette, France.
%% Date: 2024

function i=coord2state(I,p_x)

i=(I-1)'*p_x(1:length(I))+1;

