%% Tutorial example on symbolic control
%% Author: Antoine GIRARD, Université Paris-Saclay, CNRS, CentraleSupélec, Laboratoire des signaux et systèmes, 91190, Gif-sur-Yvette, France.
%% Date: 2024

function L=coordbox2state(Imin,Imax,box)

for i=1:length(Imin)
    k{i}=[Imin(i):Imax(i)];
end
L = box(k{:});
L = reshape(L,[numel(L),1]);

end
